const PHOTOS_LOADING = "PHOTOS_LOADING";
const PHOTOS_LOADED = "PHOTOS_LOADED";

export const initialPhotosState = {
    photos: [],
    loading: false,
    page: 1,
};

export let photosReducer = (state, action) => {
    switch (action.type) {
        case PHOTOS_LOADING:
            return { ...state, loading: true };
        case PHOTOS_LOADED:
            return {
                ...state,
                photos: action.payload,
                loading: false
            };
        default:
            return;
    }
};

export const photosLoadingAction = (payload) => ({ type: PHOTOS_LOADING });

export const photosLoadedAction = (payload) => ({
    type: PHOTOS_LOADED,
    payload
});