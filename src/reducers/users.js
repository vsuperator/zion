export const USERS_LOADING = "USERS_LOADING";
export const USERS_LOADED = "USERS_LOADED";
export const CHANGE_SEARCH_VALUE = "CHANGE_SEARCH_VALUE";


export const initialUsersState = {
    users: [],
    loading: false,
    page: 1,
    searchValue: '',
};

export let userReducer = (state, action) => {
    switch (action.type) {
        case USERS_LOADING:
            return { ...state, loading: true };
        case USERS_LOADED:
            return {
                ...state,
                users: action.payload,
                loading: false
            };
        case CHANGE_SEARCH_VALUE:
            return { ...state, searchValue: action.payload };
        default:
            return;
    }
};

export const changeSearchValueAction = (payload) => ({
    type: CHANGE_SEARCH_VALUE,
    payload
});

export const usersLoadingAction = (payload) => ({ type: USERS_LOADING });

export const usersLoadedAction = (payload) => ({
    type: USERS_LOADED,
    payload
});