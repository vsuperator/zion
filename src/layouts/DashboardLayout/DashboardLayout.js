import React from 'react';
import PropTypes from 'prop-types';
import Footer from '../../components/Footer';
import styles from './DashboardLayout.module.css';

const DashboardLayout = ({ children }) => (
    <div className={styles.container}>
        <div className={styles.content}>
            {children}
        </div>
        <Footer />
    </div>
);

DashboardLayout.propTypes = {
    children: PropTypes.node,
};

export default DashboardLayout;
