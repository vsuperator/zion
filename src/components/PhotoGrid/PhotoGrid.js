import React from 'react';
import PropTypes from 'prop-types';
import styles from './PhotoGrid.module.css';
import Spinner from '../Spinner';
import map from 'lodash.map';

const PhotoGrid = ({ photos, loading }) => {
    return (
        <div className={styles.container}>
            {
                loading ?
                    <Spinner /> :
                    map(photos, (photo, i) => (
                        <img
                            alt="custom"
                            key={`picture-item-${photo.id}`}
                            src={photo.urls.thumb}
                            className={styles.img}
                        />
                    ))
            }
        </div>
    );
};

PhotoGrid.propTypes = {
    photos: PropTypes.array,
};

export default PhotoGrid;
