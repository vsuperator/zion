import React from 'react';
import PropTypes from 'prop-types';
import Input from '../Input';
import styles from './SideBar.module.css';

const SideBar = ({
    searchValue,
    onChange,
    children,
}) => {
    return (
        <div className={styles.container}>
            <Input
                id="search-users"
                label="Unsplash user name"
                value={searchValue}
                onChange={onChange}
                className={styles.searchInput}
            />
            {children}
        </div>
    );
};

SideBar.propTypes = {
    searchValue: PropTypes.string,
    users: PropTypes.array,
    onChange: PropTypes.func.isRequired,
};

SideBar.defaultProps = {
    users: [],
    onChange: () => ({}),
};

export default SideBar;
