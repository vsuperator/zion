import React from 'react';
import styles from './Spinner.module.css';
import MDSpinner from "react-md-spinner";

const Spinner = () => {
    return (
        <div className={styles.container}>
            <MDSpinner />
        </div>
    );
};

export default Spinner;
