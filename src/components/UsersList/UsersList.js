import React from 'react';
import PropTypes from 'prop-types';
import styles from './UsersList.module.css';

const UsersList = ({ users, onUserClick}) => (
    <ul className={styles.list}>
        {
            users.map(user => (
                <li
                    key={`user-item-${user.id}`}
                    onClick={() => onUserClick(user.username)}
                    className={styles.user}
                >
                    {`${user.first_name} ${user.last_name}`}
                </li>
            ))
        }
    </ul>
);

UsersList.propTypes = {
    users: PropTypes.array,
    onUserClick: PropTypes.func.isRequired,
};

UsersList.defaultProps = {
    users: [],
    onUserClick: () => ({}),
};

export default UsersList;
