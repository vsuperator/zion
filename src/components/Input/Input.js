import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import Label from '../Label';
import styles from './Input.module.css';

const Input = ({
    value,
    onChange,
    label,
    className,
    id,
    type,
}) => {
    return (
        <div className={cx(styles.container, className)}>
        {label ? <Label htmlFor={id}>{label}</Label> : null}
        <input
            type={type}
            id={id}
            value={value}
            onChange={(e) => onChange(e.target.value)}
            className={styles.input}
        />
    </div>
    );
};

Input.propTypes = {
    input: PropTypes.object,
    label: PropTypes.string,
    className: PropTypes.string,
    id: PropTypes.string,
    type: PropTypes.string,
};

Input.defaultProps = {
    type: 'text',
};

export default Input;
