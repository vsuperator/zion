import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import styles from './Label.less';

const Label = ({
    children,
    className,
    htmlFor,
    ...props
}) => (<label // eslint-disable-line jsx-a11y/label-has-for
    className={cx(styles.label, className)}
    htmlFor={htmlFor}
    {...props}
>
    {children}
</label>);

Label.propTypes = {
    htmlFor: PropTypes.string.isRequired,
    className: PropTypes.string,
    children: PropTypes.node,
};

export default Label;
