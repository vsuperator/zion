import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import styles from './Footer.module.css';

const Footer = ({
    children,
    className,
    htmlFor,
    ...props
}) => (<footer // eslint-disable-line jsx-a11y/Footer-has-for
    className={cx(styles.footer, className)}
>
    Footer.
</footer>);

Footer.propTypes = {
    className: PropTypes.string,
    children: PropTypes.node,
};

export default Footer;
