import React from 'react';
import UsersList from '../../components/UsersList'
import Spinner from '../../components/Spinner'

export default function SideBarUsers({ users, loading, searchValue, onUserClick }) {
    const usersCount = users.length;
    if (loading) {
        return <Spinner />
    }
    if (usersCount === 0) {
        if (!searchValue) {
            return <span>
                Please type a username to search.
            </span>
        }
        return (
            <span>
                There is no users for <b>{searchValue}</b>
            </span>
        );
    }
    return (
        <UsersList
            users={users}
            onUserClick={onUserClick}
        />
    );
}
