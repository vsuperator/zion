import React, { useReducer, useEffect } from 'react';
import Unsplash, { toJson } from "unsplash-js";
import DashboardLayout from '../../layouts/DashboardLayout'
import SideBar from '../../components/SideBar'
import PhotoGrid from '../../components/PhotoGrid'
import { useDebounce } from '../../utils/useDebounce';
import SideBarUsers from './SideBarUsers';
import {
    changeSearchValueAction,
    usersLoadingAction,
    usersLoadedAction,
    userReducer,
    initialUsersState,
} from '../../reducers/users';
import {
    photosLoadingAction,
    photosReducer,
    initialPhotosState,
    photosLoadedAction,
} from '../../reducers/photos';

const unsplash = new Unsplash({
    applicationId: "aa2f3c3be8125f1fc86e3007153420c4e446c19b7b0c6d80a6257b281c9a0dc5",
    secret: "a5ab4ed2efdc772dca8d5636a26c0d897907df38cd92baa9067e57093d9596b5"
});



function Dashboard() {
    const [usersState, usersDispatch] = useReducer(userReducer, initialUsersState);
    const [photosState, photosDispatch] = useReducer(photosReducer, initialPhotosState);


    const debouncedUserSearch = useDebounce(usersState.searchValue, 500);

    useEffect(
        () => {
            if (debouncedUserSearch) {
                usersDispatch(usersLoadingAction());
                unsplash.search.users(debouncedUserSearch, 1, 25)
                    .then(toJson)
                    .then(json => {
                        usersDispatch(usersLoadedAction(json.results));
                    });
            }
        },
        [debouncedUserSearch]
    );

    function handleSelectUser(username) {
        photosDispatch(photosLoadingAction());
        unsplash.users.photos(username, 1, 9, "popular")
            .then(toJson)
            .then(photos => photosDispatch(photosLoadedAction(photos)));
    }
    return (
        <DashboardLayout>
            <SideBar
                searchValue={usersState.searchValue}
                onChange={(value) => usersDispatch(changeSearchValueAction(value))}
            >
                <SideBarUsers
                    users={usersState.users}
                    loading={usersState.loading}
                    searchValue={usersState.searchValue}
                    onUserClick={handleSelectUser}
                />
            </SideBar>
            <PhotoGrid
                loading={photosState.loading}
                photos={photosState.photos}
            />
        </DashboardLayout>
    );
}

export default Dashboard;
